﻿using Newtonsoft.Json;
using System;

namespace Common.DTO
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                TaskDTO secondTaskDTO = (TaskDTO)obj;
                if (Id == secondTaskDTO.Id
                    && ProjectId == secondTaskDTO.ProjectId
                    && PerformerId == secondTaskDTO.PerformerId
                    && Name == secondTaskDTO.Name
                    && Description == secondTaskDTO.Description
                    && State == secondTaskDTO.State
                    && CreatedAt == secondTaskDTO.CreatedAt
                    && FinishedAt == secondTaskDTO.FinishedAt)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
