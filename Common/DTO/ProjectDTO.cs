﻿using System;

namespace Common.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                ProjectDTO secondProjectDTO = (ProjectDTO)obj;
                if (Id == secondProjectDTO.Id
                    && TeamId == secondProjectDTO.TeamId
                    && Name == secondProjectDTO.Name
                    && AuthorId == secondProjectDTO.AuthorId
                    && Description == secondProjectDTO.Description
                    && Deadline == secondProjectDTO.Deadline
                    && CreatedAt == secondProjectDTO.CreatedAt)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
