﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.Models
{
    public class User : Entity
    {
        public int TeamId { get; set; }
        [MaxLength(10), MinLength(1)]
        public string Name { get; set; }
        [MaxLength(15), MinLength(1)]
        public string Surname { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public override string ToString()
        {
            return string.Format("\nUser: [Id:{0}, TeamId:{1}, FirstName:{2}, LastName:{3}, Email:{4}, RegisteredAt:{5}, BirthDay:{6} ]\n", Id, TeamId, Name, Surname, Email, RegisteredAt, BirthDay);
        }
    }
}
