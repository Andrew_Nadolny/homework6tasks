﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.Models
{
    public class Team : Entity
    {
        public int ProjectId { get; set; }
        [MaxLength(30), MinLength(1)]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return string.Format("\nTeam: [ Id:{0}, Name:{1}, CreatedAt:{2} ]\n", Id, Name, CreatedAt);
        }
    }
}
