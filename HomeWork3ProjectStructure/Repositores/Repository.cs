﻿using Common.Models;
using HomeWork3ProjectStructure.DAL;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Repositores
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        public ProjectsDbContext _context;

        public Repository(ProjectsDbContext context)
        {
            _context = context;
            //context.ChangeTracker.Clear();
        }
        public async Task<List<TEntity>> GetAsync()
        {
            return await _context.Set<TEntity>().AsNoTracking().ToListAsync();
        }

        public async Task<TEntity> GetAsync(int id)
        {
            return await _context.Set<TEntity>().AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            return (await _context.Set<TEntity>().AddAsync(entity)).Entity;
        }

        public async Task<bool> DeleteAsync(TEntity entity)
        {
            await System.Threading.Tasks.Task.Run(() => _context.Set<TEntity>().Remove(entity));
            return await _context.Set<TEntity>().AnyAsync(x => x == entity);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await System.Threading.Tasks.Task.Run(() => _context.Set<TEntity>().Remove(_context.Set<TEntity>().SingleOrDefault(x => x.Id == id)));
            return await _context.Set<TEntity>().AnyAsync(x => x.Id == id);
        }


        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            return await System.Threading.Tasks.Task.Run(() => _context.Set<TEntity>().Update(entity).Entity);
        }

    }
}
