﻿using Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace HomeWork3ProjectStructure.Repositores
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task<List<TEntity>> GetAsync();

        Task<TEntity> GetAsync(int Id);

        Task<TEntity> CreateAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);

        Task<bool> DeleteAsync(int id);

        Task<bool> DeleteAsync(TEntity entity);
    }
}
