﻿using Common.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace HomeWork3ProjectStructure.DAL
{
    public class ProjectsDbContext : DbContext, IDisposable
    {
        public ProjectsDbContext(DbContextOptions<ProjectsDbContext> options) : base(options) { }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Common.Models.Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(new User() { Id = 1, TeamId = 10, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") });
            modelBuilder.Entity<Team>().HasData(new Team() { Id = 10, ProjectId = 1, Name = "Denesik - Greenfelder", CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00") });
            modelBuilder.Entity<Project>().HasData(
                new Project()
                {
                    Id = 1,
                    AuthorId = 1,
                    TeamId = 10,
                    Name = "backing up Handcrafted Fresh Shoes challenge",
                    Description = "Et doloribus et temporibus.",
                    Deadline = Convert.ToDateTime("2021-09-12T19:17:47.2335223+00:00"),
                    CreatedAt = Convert.ToDateTime("2020-08-25T17:49:50.4518054+00:00")
                });
            modelBuilder.Entity<Common.Models.Task>().HasData(new List<Common.Models.Task>() { new Common.Models.Task() { Id = 1, ProjectId = 1, PerformerId = 1, Name = "real-time", Description = "Quo sint aut et ea voluptatem omnis ut.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 2, ProjectId = 1, PerformerId = 1,  Name = "product Direct utilize", Description = "Eum a eum.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2019-02-15T15:06:52.0600666+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 3, ProjectId = 1, PerformerId = 1, Name = "bypass", Description = "Sint voluptatem quas.", State = TaskState.Done, CreatedAt = Convert.ToDateTime("2017-08-16T06:13:44.5773845+00:00"), FinishedAt = Convert.ToDateTime("2021-09-09T06:34:47.460216+00:00") },
                    new Common.Models.Task() { Id = 4, ProjectId = 1, PerformerId = 1, Name = "withdrawal contextually-based", Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2018-10-19T00:58:34.8045103+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 5, ProjectId = 1, PerformerId = 1, Name = "mobile Organized", Description = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2018-06-15T06:03:48.0732466+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 6, ProjectId = 1, PerformerId = 1, Name = "world-class Circles", Description = "Reiciendis iusto rerum non et aut eaque.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2020-05-21T14:56:53.8117818+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 7, ProjectId = 1, PerformerId = 1, Name = "Automotive & Tools transitional bifurcated", Description = "Et rerum ad.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2018-07-10T16:21:12.0886153+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 8, ProjectId = 1, PerformerId = 1, Name = "payment methodologies", Description = "Voluptas nostrum sint.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2019-05-07T20:29:10.058295+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 9, ProjectId = 1, PerformerId = 1, Name = "Borders Mountain", Description = "Rerum iure soluta consequatur velit aut.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2020-06-19T11:42:55.0738847+00:00"), FinishedAt = Convert.ToDateTime(null) } });

            base.OnModelCreating(modelBuilder);
        }
    }
}
