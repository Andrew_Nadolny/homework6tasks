﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteTaskByItemCommand : ICommand<Task<bool>>
    {
        public TaskDTO task { get; set; }
    }
}
