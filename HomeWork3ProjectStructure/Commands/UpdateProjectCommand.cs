﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class UpdateProjectCommand : ICommand<Task<ProjectDTO>>
    {
        public ProjectDTO project { get; set; }
    }
}
