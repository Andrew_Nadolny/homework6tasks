﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class UpdateTeamCommand : ICommand<Task<TeamDTO>>
    {
        public TeamDTO team { get; set; }
    }
}
