﻿using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Interfaces
{
    public interface ICommandHandler<TCommand, TResult> where TCommand : ICommand<TResult>
    {
        public TResult HandlerAsync(TCommand command);
    }
}
