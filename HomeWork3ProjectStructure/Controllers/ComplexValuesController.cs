﻿using AutoMapper;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexValuesController : ControllerBase
    {

        private readonly QuerieProcessor _querieProcessor;

        public ComplexValuesController(ProjectsDbContext _context, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(_context, mapper));

        }

        [Route("GetCountTasksInProjectByAuthorId")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetCountTasksInProjectByAuthorIdAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetCountTasksInProjectByAuthorIdQuerie() { Id = id }));
        }

        [Route("GetListOfTeamsWithTeamatesOlderThen10")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetListOfTeamsWithTeamatesOlderThen10Async()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetListOfTeamsWithTeamatesOlderThen10Querie()));
        }

        [Route("GetListOfUsersAscendingWhitTasksDescending")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetListOfUsersAscendingWhitTasksDescendingAsync()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetListOfUsersAscendingWhitTasksDescendingQuerie()));
        }

        [Route("GetProjectWithTaskWithLongestDescriptionsAndETC")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetProjectWithTaskWithLongestDescriptionsAndETCAsync()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetProjectWithTaskWithLongestDescriptionsAndETCQuerie()));
        }

        [Route("GetUserWithLastProjectAndETC")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetUserWithLastProjectAndETCAsync()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.Processed(new GetUserWithLastProjectAndETCQuerie()));
        }


        [Route("GetUserTasksWithShortNameByUserId")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetUserTasksWithShortNameByUserIdAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUserTasksWithShortNameByUserIdQuerie() { Id = id }));
        }

        [Route("GetUserTasksFinishedInCurrentYear")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<string> GetUserTasksFinishedInCurrentYearAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUserTasksFinishedInCurrentYearQuerie() { Id = id }));
        }

        [Route("GetUserUnfinishedTasks")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async System.Threading.Tasks.Task<IActionResult> GetUserUnfinishedTasksAsync(int id)
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUserUnfinishedTasks() { Id = id })));
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (AggregateException)
            {
                return NoContent();
            }
        }
    }
}
