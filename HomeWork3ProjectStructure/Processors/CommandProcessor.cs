﻿using Common.DTO;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Processors
{
    public class CommandProcessor : ICommandProcessor
    {
        private CommandHandler _commandHandler { get; set; }
        public CommandProcessor(CommandHandler commandHandler)
        {
            _commandHandler = commandHandler;
        }

        public async Task<ProjectDTO> ProcessedAsync(CreateProjectCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<TaskDTO> ProcessedAsync(CreateTaskCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<TeamDTO> ProcessedAsync(CreateTeamCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<UserDTO> ProcessedAsync(CreateUserCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<ProjectDTO> ProcessedAsync(UpdateProjectCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<TaskDTO> ProcessedAsync(UpdateTaskCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<TeamDTO> ProcessedAsync(UpdateTeamCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<UserDTO> ProcessedAsync(UpdateUserCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<bool> ProcessedAsync(DeleteProjectByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTaskByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTeamByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteUserByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteProjectByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTaskByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTeamtByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteUserByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
    }
}
