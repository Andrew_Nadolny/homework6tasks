﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeWork3ProjectStructure.Migrations
{
    public partial class UpdateSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                column: "TeamId",
                value: 10);

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "FinishedAt", "State" },
                values: new object[] { new DateTime(2021, 9, 9, 9, 34, 47, 460, DateTimeKind.Local).AddTicks(2160), 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                column: "TeamId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "FinishedAt", "State" },
                values: new object[] { new DateTime(2020, 9, 9, 9, 34, 47, 460, DateTimeKind.Local).AddTicks(2160), 1 });
        }
    }
}
