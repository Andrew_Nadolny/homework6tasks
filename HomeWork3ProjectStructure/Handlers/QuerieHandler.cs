﻿using AutoMapper;
using Common.DTO;
using Common.Models;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Interfaces;
using HomeWork3ProjectStructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Handlers
{
    public class QuerieHandler : IQuerieHandler<GetProjectsQuerie, Task<List<ProjectDTO>>>,
        IQuerieHandler<GetTasksQuerie, Task<List<TaskDTO>>>,
        IQuerieHandler<GetTeamsQuerie, Task<List<TeamDTO>>>,
        IQuerieHandler<GetUsersQuerie, Task<List<UserDTO>>>,
        IQuerieHandler<GetProjectByIdQuerie, Task<ProjectDTO>>,
        IQuerieHandler<GetTaskByIdQuerie, Task<TaskDTO>>,
        IQuerieHandler<GetTeamByIdQuerie, Task<TeamDTO>>,
        IQuerieHandler<GetCountTasksInProjectByAuthorIdQuerie, Task<Dictionary<int, int>>>,
        IQuerieHandler<GetListOfTeamsWithTeamatesOlderThen10Querie, Task<List<(int Id, string Name, List<UserDTO> Teammates)>>>,
        IQuerieHandler<GetListOfUsersAscendingWhitTasksDescendingQuerie, Task<List<(UserDTO User, List<TaskDTO> Tasks)>>>,
        IQuerieHandler<GetProjectWithTaskWithLongestDescriptionsAndETCQuerie, Task<List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>>>,
        IQuerieHandler<GetUserTasksFinishedInCurrentYearQuerie, Task<List<TaskDTO>>>,
        IQuerieHandler<GetUserTasksWithShortNameByUserIdQuerie, Task<List<TaskDTO>>>,
        IQuerieHandler<GetUserWithLastProjectAndETCQuerie, Task<List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)>>>,
        IQuerieHandler<GetUserUnfinishedTasks, Task<List<TaskDTO>>>
    {
        private readonly UnitOfWork.UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public QuerieHandler(ProjectsDbContext context, IMapper mapper)
        {
            _unitOfWork = new UnitOfWork.UnitOfWork(context);
            _mapper = mapper;
        }
        public async Task<List<Project>> HandleAsync(GetProjectsStructureQuerie querie)
        {
            List<Project> projects = await _unitOfWork.Set<Project>().GetAsync();
            List<Team> teams = await _unitOfWork.Set<Team>().GetAsync();
            List<Common.Models.Task> tasks = await _unitOfWork.Set<Common.Models.Task>().GetAsync();
            List<User> users = await _unitOfWork.Set<User>().GetAsync();
            if (projects != null && teams != null && tasks != null && users != null)
            {
                return projects.Select
                    (p => new Project { Id = p.Id, AuthorId = p.AuthorId, CreatedAt = p.CreatedAt, Deadline = p.Deadline, Description = p.Description, Name = p.Name, TeamId = p.TeamId, Tasks = tasks.Where(x => x.ProjectId == p.Id).ToList() }).ToList();
            }
            return null;

        }
        public async Task<List<ProjectDTO>> HandleAsync(GetProjectsQuerie querie)
        {
            return _mapper.Map<List<ProjectDTO>>(await _unitOfWork.Set<Project>().GetAsync());
        }
        public async Task<List<TaskDTO>> HandleAsync(GetTasksQuerie querie)
        {
            return _mapper.Map<List<TaskDTO>>(await _unitOfWork.Set<Common.Models.Task>().GetAsync());
        }
        public async Task<List<TeamDTO>> HandleAsync(GetTeamsQuerie querie)
        {
            return _mapper.Map<List<TeamDTO>>(await _unitOfWork.Set<Team>().GetAsync());
        }
        public async Task<List<UserDTO>> HandleAsync(GetUsersQuerie querie)
        {
            return _mapper.Map<List<UserDTO>>(await _unitOfWork.Set<User>().GetAsync());
        }
        public async Task<ProjectDTO> HandleAsync(GetProjectByIdQuerie querie)
        {
            return _mapper.Map<ProjectDTO>(await _unitOfWork.Set<Project>().GetAsync(querie.Id));
        }
        public async Task<TaskDTO> HandleAsync(GetTaskByIdQuerie querie)
        {
            return _mapper.Map<TaskDTO>(await _unitOfWork.Set<Common.Models.Task>().GetAsync(querie.Id));
        }
        public async Task<TeamDTO> HandleAsync(GetTeamByIdQuerie querie)
        {
            return _mapper.Map<TeamDTO>(await _unitOfWork.Set<Team>().GetAsync(querie.Id));
        }
        public async Task<UserDTO> HandleAsync(GetUserByIdQuerie querie)
        {
            return _mapper.Map<UserDTO>(await _unitOfWork.Set<User>().GetAsync(querie.Id));
        }
        public async Task<Dictionary<int, int>> HandleAsync(GetCountTasksInProjectByAuthorIdQuerie querie)
        {
            var tasks = await _unitOfWork.Set<Common.Models.Task>().GetAsync();
            return (await _unitOfWork.Set<Project>().GetAsync()).Where(x => x.AuthorId == querie.Id).ToDictionary(x => x.Id, x => tasks.Where(task => task.ProjectId == x.Id && task.PerformerId == querie.Id).Count());
        }
        public async Task<List<(int Id, string Name, List<UserDTO> Teammates)>> HandleAsync(GetListOfTeamsWithTeamatesOlderThen10Querie querie)
        {
            List<UserDTO> users =  _mapper.Map<List<UserDTO>>(await _unitOfWork.Set<User>().GetAsync());
            return (await _unitOfWork.Set<Team>().GetAsync())
                .Select( x =>
                 (Id: x.Id,
                 Name: x.Name,
                 Teammates: users?.Where(x => x.TeamId == x.TeamId).OrderBy(x => x.RegisteredAt).ToList())
                 ).Where(x => !x.Teammates.Any(x => x.BirthDay > DateTime.Now.AddYears(-10))).ToList();
        }
        public async Task<List<(UserDTO User, List<TaskDTO> Tasks)>> HandleAsync(GetListOfUsersAscendingWhitTasksDescendingQuerie querie)
        {
            var users = await _unitOfWork.Set<User>().GetAsync();
            var tasks = await _unitOfWork.Set<Common.Models.Task>().GetAsync();

            return users.Distinct().OrderBy(x => x.Name.Length)
                .Select(
                user => (
                    User: _mapper.Map<UserDTO>(user),
                    Tasks: _mapper.Map<List<TaskDTO>>(tasks.Where(x => x.PerformerId == user.Id).OrderByDescending(x => x.Name.Length).ToList())
            )).ToList();
        }
        public async Task<List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>>
            HandleAsync(GetProjectWithTaskWithLongestDescriptionsAndETCQuerie querie)
        {
            var projects = await HandleAsync(new GetProjectsStructureQuerie());
            var user = await _unitOfWork.Set<User>().GetAsync();
            return projects.Select(project => (
            Project: _mapper.Map<ProjectDTO>(project),
            TaskWithLongestDescriptions: _mapper.Map<TaskDTO>(project.Tasks.DefaultIfEmpty().Aggregate((longest, next) =>
                        next.Description.Length > longest.Description.Length ? next : longest)),
            TaskWithShortestName: _mapper.Map<TaskDTO>(project.Tasks.DefaultIfEmpty().Aggregate((shortests, next) =>
                       next.Name.Length < shortests.Name.Length ? next : shortests)),
            CountOfUsersInProjects: (project.Description.Length > 20 || project.Tasks.Count() < 3) ? user.Where(x => x.TeamId == project.TeamId).Count() : 0
            )).ToList();
        }
        public async Task<List<TaskDTO>> HandleAsync(GetUserTasksFinishedInCurrentYearQuerie querie)
        {
            return (await HandleAsync(new GetTasksQuerie())).Where(x => x.FinishedAt.Value.Year == DateTime.Now.Year && x.PerformerId == querie.Id).ToList();
        }
        public async Task<List<TaskDTO>> HandleAsync(GetUserTasksWithShortNameByUserIdQuerie querie)
        {
            if ((await _unitOfWork.Set<User>().GetAsync(querie.Id)) == null)
            {
                return new List<TaskDTO>();
            }
            return (await HandleAsync(new GetTasksQuerie())).Where(x => x.PerformerId == querie.Id && x.Name.Length < 45).ToList();
        }
        public async Task<List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)>>
            HandleAsync(GetUserWithLastProjectAndETCQuerie querie)
        {
            var projects = await HandleAsync(new GetProjectsStructureQuerie());
            var teams = await _unitOfWork.Set<Team>().GetAsync();
            var tasks = await _unitOfWork.Set<Common.Models.Task>().GetAsync();
            var users = await _unitOfWork.Set<User>().GetAsync();


            return users.Select(user => (
               User: _mapper.Map<UserDTO>(user),
               LastProject: _mapper.Map<ProjectDTO>(projects.Where(project => project.AuthorId == user.Id).OrderBy(project => project.CreatedAt)?.FirstOrDefault()),
               LastProjectTask: _mapper.Map<List<TaskDTO>>(projects.Where(project => project.AuthorId == user.Id).OrderBy(project => project.CreatedAt).FirstOrDefault()?.Tasks),
               CountOfUnfinishedAndCanceledTasks: tasks.Where(x => x.PerformerId == user.Id && (x.FinishedAt != DateTime.MinValue || x.State == TaskState.Canceled)).Count(),
               LongesUserTask: _mapper.Map<TaskDTO>(tasks.Where(task => task.PerformerId == user.Id).DefaultIfEmpty().Aggregate((longest, next) =>
                      (next.FinishedAt - next.CreatedAt) > (longest.FinishedAt - longest.CreatedAt) ? next : longest))
           )).ToList();
        }
        public async Task<List<TaskDTO>> HandleAsync(GetUserUnfinishedTasks querie)
        {
            var user = await _unitOfWork.Set<User>().GetAsync(querie.Id);
            if (user == null)
            {
                throw new ArgumentException();
            }
            var usersTasks = (await _unitOfWork.Set<Common.Models.Task>().GetAsync()).Where(x => x.PerformerId == querie.Id);
            if (usersTasks.Count() == 0)
            {
                throw new AggregateException();
            }
            return _mapper.Map<List<TaskDTO>>(usersTasks.Where(x => x.State != TaskState.Done).ToList());
        }
    }
}
