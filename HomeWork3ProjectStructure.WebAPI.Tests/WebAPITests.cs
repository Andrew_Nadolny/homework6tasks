using Common.DTO;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace HomeWork3ProjectStructure.WebAPI.Tests
{
    public class WebAPITests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public WebAPITests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient();
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserTasksFinishedInCurrentYearQuerie_WhenUserExsist_ThenListOfTasks()
        {
            var responce = await _client.GetAsync(string.Format("api/complexvalues/GetUserTasksFinishedInCurrentYear?Id={0}", 1));

            var expretedTask = new TaskDTO() { Id = 3, ProjectId = 1, PerformerId = 1, Name = "bypass", Description = "Sint voluptatem quas.", State = 1, CreatedAt = Convert.ToDateTime("2017-08-16T06:13:44.5773845+00:00"), FinishedAt = Convert.ToDateTime("2021-09-09T06:34:47.460216+00:00") };
            var stringResponce = await responce.Content.ReadAsStringAsync();
            var responceList = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponce);

            Assert.Collection(responceList,
                item => Assert.True(item.Equals(expretedTask))
                );
        }
        [Fact]
        public async System.Threading.Tasks.Task CreatePoject_WhenProjectUnexisit_ThenOK()
        {
            var newProject = new ProjectDTO()
            {
                AuthorId = 1,
                TeamId = 10,
                Name = "backing up Handcrafted Fresh Shoes challenge",
                Description = "Et doloribus et temporibus.",
                Deadline = Convert.ToDateTime("2021-09-12T19:17:47.2335223+00:00"),
                CreatedAt = Convert.ToDateTime("2020-08-25T17:49:50.4518054+00:00")
            };

            var responce = await _client.PostAsync("api/project", new StringContent(JsonConvert.SerializeObject(newProject), Encoding.UTF8, "application/json"));
            var stringResponce = await responce.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
            var responceProject = JsonConvert.DeserializeObject<ProjectDTO>(stringResponce);
            newProject.Id = responceProject.Id;
            Assert.True(newProject.Equals(responceProject));
        }

        [Fact]
        public async System.Threading.Tasks.Task CreatePoject_WhenProjectExisit_ThenBadRequset()
        {
            var newProject = new ProjectDTO()
            {
                Id = 1,
                AuthorId = 1,
                TeamId = 10,
                Name = "backing up Handcrafted Fresh Shoes challenge",
                Description = "Et doloribus et temporibus.",
                Deadline = Convert.ToDateTime("2021-09-12T19:17:47.2335223+00:00"),
                CreatedAt = Convert.ToDateTime("2020-08-25T17:49:50.4518054+00:00")
            };

            var responce = await _client.PostAsync("api/project", new StringContent(JsonConvert.SerializeObject(newProject), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.BadRequest, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteUserById_WhenUserExsist_ThenOK()
        {
            var responce = await _client.DeleteAsync(string.Format("api/user/{0}", 1));
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteUserById_WhenUserUnexsist_ThenNoContent()
        {
            var responce = await _client.DeleteAsync(string.Format("api/user/{0}", 2));
            Assert.Equal(HttpStatusCode.NoContent, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteTask_WhenTaskExsist_ThenOK()
        {
            var task = new TaskDTO() { Id = 2, ProjectId = 1, PerformerId = 1, Name = "product Direct utilize", Description = "Eum a eum.", State = 1, CreatedAt = Convert.ToDateTime("2019-02-15T15:06:52.0600666+00:00"), FinishedAt = Convert.ToDateTime(null) };

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, "api/task");
            request.Content = new StringContent(JsonConvert.SerializeObject(task), Encoding.UTF8, "application/json");
            var responce = await _client.SendAsync(request);
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteTask_WhenTaskUnexsist_ThenNoContent()
        {
            var task = new TaskDTO() { Id = 12, ProjectId = 1, PerformerId = 1, Name = "product Direct utilize", Description = "Eum a eum.", State = 1, CreatedAt = Convert.ToDateTime("2019-02-15T15:06:52.0600666+00:00"), FinishedAt = Convert.ToDateTime(null) };

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, "api/task");
            request.Content = new StringContent(JsonConvert.SerializeObject(task), Encoding.UTF8, "application/json");
            var responce = await _client.SendAsync(request);
            Assert.Equal(HttpStatusCode.NoContent, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateTeam_WhenTeamUnexisit_ThenOK()
        {
            var newTeam = new TeamDTO() { Name = "Denesik - Greenfelder", CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00"), };

            var responce = await _client.PostAsync("api/team", new StringContent(JsonConvert.SerializeObject(newTeam), Encoding.UTF8, "application/json"));
            var stringResponce = await responce.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
            var responceTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponce);
            newTeam.Id = responceTeam.Id;
            Assert.True(newTeam.Equals(responceTeam));
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateTeam_WhenTeamExisit_ThenBadRequset()
        {
            var newTeam = new TeamDTO() { Id = 10, Name = "Denesik - Greenfelder", CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00"), };

            var responce = await _client.PostAsync("api/team", new StringContent(JsonConvert.SerializeObject(newTeam), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.BadRequest, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserTasksFinishedInCurrentYearQuerie_WhenUserUnexsist_ThenEmptyList()
        {
            var responce = await _client.GetAsync(string.Format("api/complexvalues/GetUserTasksFinishedInCurrentYear?Id={0}", 2));

            var stringResponce = await responce.Content.ReadAsStringAsync();
            var responceTaskList = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponce);

            Assert.Empty(responceTaskList);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserUnexsist_ThenBadRequest()
        {
            var responce = await _client.GetAsync(string.Format("api/complexvalues/GetUserUnfinishedTasks?Id={0}", 2));

            Assert.Equal(HttpStatusCode.BadRequest, responce.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserExsist_ThenListOfTasks()
        {
            var responce = await _client.GetAsync(string.Format("api/complexvalues/GetUserUnfinishedTasks?Id={0}", 1));
            var stringResponce = await responce.Content.ReadAsStringAsync();
            var responceTaskList = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponce);

            Assert.True(responceTaskList.All(x => x.State != 2));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUnFinishedTasksUnExsist_ThenEmptyListOfTasks()
        {
            var newUser = new User() { Id = 2, TeamId = 10, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") };

            var responceAddNewUser = await _client.PostAsync("api/user", new StringContent(JsonConvert.SerializeObject(newUser), Encoding.UTF8, "application/json"));
            var stringResponceAddNewUser = await responceAddNewUser.Content.ReadAsStringAsync();
            var responceNewUser = JsonConvert.DeserializeObject<UserDTO>(stringResponceAddNewUser);

            var task = new TaskDTO() { Id = 10, ProjectId = 1, PerformerId = responceNewUser.Id, Name = "product Direct utilize", Description = "Eum a eum.", State = 2, CreatedAt = Convert.ToDateTime("2019-02-15T15:06:52.0600666+00:00"), FinishedAt = Convert.ToDateTime("2019-03-15T15:06:52.0600666+00:00") };
            await _client.PostAsync("api/task", new StringContent(JsonConvert.SerializeObject(task), Encoding.UTF8, "application/json"));

            var responce = await _client.GetAsync(string.Format("api/complexvalues/GetUserUnfinishedTasks?Id={0}", responceNewUser.Id));
            var stringResponce = await responce.Content.ReadAsStringAsync();
            var responceTaskList = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponce);

            Assert.Empty(responceTaskList);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserTasksUnExsist_ThenEmptyListOfTasks()
        {
            var newUser = new User() { TeamId = 10, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") };

            var responceAddNewUser = await _client.PostAsync("api/user", new StringContent(JsonConvert.SerializeObject(newUser), Encoding.UTF8, "application/json"));
            var stringResponceAddNewUser = await responceAddNewUser.Content.ReadAsStringAsync();
            var responceNewUser = JsonConvert.DeserializeObject<UserDTO>(stringResponceAddNewUser);

            var responce = await _client.GetAsync(string.Format("api/complexvalues/GetUserUnfinishedTasks?Id={0}", responceNewUser.Id));

            Assert.Equal(HttpStatusCode.NoContent, responce.StatusCode);
        }
    }
}
