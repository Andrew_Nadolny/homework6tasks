﻿using Common.DTO;
using Common.Models;
using ConsoleClient.Reguests;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleClient
{
    public class QueriesService
    {
        public System.Threading.Tasks.Task<int> MarkRandomTaskWithDelay(int delay)
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            System.Threading.Tasks.Task.Run(() =>
            {
                Timer timer = new Timer(delay);

                timer.Elapsed += new ElapsedEventHandler(async (timerSender, elapsedEventArgs) =>
                    {
                        TaskRequest taskRequest = new TaskRequest();
                        var tasks = await taskRequest.GetTasksAsync();
                        if (tasks.Count > 0)
                        {
                            var task = tasks?.ElementAt(new Random().Next(0, tasks.Count()));
                            task.State = TaskState.Done;
                            var updatedTask = await taskRequest.UpdateTaskAsync(task);
                            tcs.SetResult(updatedTask.Id);
                        }
                    }
                );
                timer.AutoReset = false;
                timer.Start();

            });
            return tcs.Task;
        }
        //public System.Threading.Tasks.Task<int> MarkRandomTaskWithDelay(int delay)
        //{
        //    TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
        //    BackgroundWorker worker = new BackgroundWorker();
        //    worker.DoWork += (workerSender, doWorkEventArgs) =>
        //    {
        //        Timer timer = new Timer(delay);
        //        timer.Elapsed += new ElapsedEventHandler(async (timerSender, elapsedEventArgs) =>
        //        {
        //            TaskRequest taskRequest = new TaskRequest();
        //            var tasks = await taskRequest.GetTasksAsync();
        //            var task = tasks.ElementAt(new Random().Next(0, tasks.Count() - 1));
        //            task.State = 3;
        //            doWorkEventArgs.Result = await taskRequest.UpdateTaskAsync(task);
        //        }
        //        );
        //        timer.AutoReset = false;
        //        timer.Start();
        //    };
        //    worker.RunWorkerCompleted += (workerSender, doWorkEventArgs) =>
        //    {
        //        if (doWorkEventArgs.Error != null)
        //        {
        //            tcs.SetException(doWorkEventArgs.Error);
        //        }
        //        else
        //        {
        //            tcs.SetResult((int)doWorkEventArgs.Result);
        //        }
        //    };
        //    worker.RunWorkerAsync();
        //    return tcs.Task;
        //}
    }
}
