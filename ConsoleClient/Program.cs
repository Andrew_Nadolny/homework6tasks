﻿using ConsoleClient.Reguests;
using System;
using System.Linq;

namespace ConsoleClient
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            //int markedTaskId = 0;
            var complexValueRequest = new ComplexValueRequest();
            
            //var projects = await complexValueRequest.GetProjectsStructureQuerie();
            while (true)
            {
                
                Console.WriteLine("1. Get the number of tasks from the project of a specific user. ");

                Console.WriteLine("2. Get a list of tasks assigned to a specific user, where name is a task < 45 characters(a collection of tasks). ");
                Console.WriteLine("3. Get the tasks that  are finished(finished) in the current(2021) year for a  specific user(by id). ");
                Console.WriteLine("4 .Get a list(id, team name and list of users) from teams whose members are over 10 years old, sorted by user registration date in descending order,  and also grouped by teams .");
                Console.WriteLine("5. Get a list of users alphabetically sorted(ascending) with tasks sorted by name length(descending). ");
                Console.WriteLine("6. Get the following structure(pass user Id to parameters): User, User's last project (by creation date), Total number of tasks under the last project, Total number of unfinished or canceled tasks for the user, Longest user task by date(first created - last completed). ");
                Console.WriteLine("7.Get the following structure: Project, The longest task of the project(by description), Shortest project task(by name), The total number of users in the project team, where either the project description > 20 characters or the number of tasks < 3");
                Console.WriteLine("***NEW!!!! 8.Mark a random task as done. ");
                Console.WriteLine("\nEnter the menu item number: ");
                int selectedItem = 0;
                if (int.TryParse(Console.ReadLine(), out selectedItem))
                {
                    int selectedUser = 0;
                    if (selectedItem < 4)
                    {
                        Console.WriteLine("Enter user Id:");
                        if (int.TryParse(Console.ReadLine(), out selectedUser))
                        {
                            if (!((new UserRequest().GetUserAsync(selectedUser)) != null))
                            {
                                Console.WriteLine("User not found.");
                                Console.ReadKey();
                                Console.Clear();
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("The entered value is not a valid numeric value.");
                            Console.ReadKey();
                            Console.Clear();
                            continue;
                        }
                    }
                    switch (selectedItem)
                    {
                        case 1:
                            var CountTasksInProjectByAuthorId = await complexValueRequest.GetCountTasksInProjectByAuthorId(selectedUser);
                            if (CountTasksInProjectByAuthorId == null || CountTasksInProjectByAuthorId?.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var pair in CountTasksInProjectByAuthorId)
                            {
                                Console.WriteLine("ProjectId: " + pair.Key + " Number of tasks:" + pair.Value);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 2:
                            var UserTasksWithShortNameByUserId = await complexValueRequest.GetUserTasksWithShortNameByUserId(selectedUser);
                            if (UserTasksWithShortNameByUserId == null || UserTasksWithShortNameByUserId?.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in UserTasksWithShortNameByUserId)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 3:
                            var UserTasksFinishedInCurrentYear = await complexValueRequest.GetUserTasksFinishedInCurrentYear(selectedUser);
                            if (UserTasksFinishedInCurrentYear == null || UserTasksFinishedInCurrentYear.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in UserTasksFinishedInCurrentYear)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 4:
                            var ListOfTeamsWithTeamatesOlderThen10 = await complexValueRequest.GetListOfTeamsWithTeamatesOlderThen10();
                            if (ListOfTeamsWithTeamatesOlderThen10 == null || ListOfTeamsWithTeamatesOlderThen10?.Count() == 0)
                            {
                                Console.WriteLine("No teams");
                                break;
                            }
                            foreach (var team in ListOfTeamsWithTeamatesOlderThen10)
                            {
                                string userInfo = "";
                                foreach (var teammate in team.Teammates)
                                {
                                    userInfo += teammate.ToString() + " ";
                                }
                                Console.WriteLine(string.Format(" Team: [ Id:{0}, Name:{1}, Users:{2} ] ", team.Id, team.Name, userInfo));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
                            break;
                        case 5:
                            var ListOfUsersAscendingWhitTasksDescending = await complexValueRequest.GetListOfUsersAscendingWhitTasksDescending();
                            if (ListOfUsersAscendingWhitTasksDescending == null || ListOfUsersAscendingWhitTasksDescending?.Count() == 0)
                            {
                                Console.WriteLine("No results");
                                break;
                            }
                            foreach (var tuple in ListOfUsersAscendingWhitTasksDescending)
                            {
                                string taskInfo = "";
                                foreach (var task in tuple.Tasks)
                                {
                                    taskInfo += task.ToString() + " ";
                                }
                                Console.WriteLine(string.Format(" Tuple: [ User:{0}, Tasks:{1} ] ", tuple.User, taskInfo));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
                            break;
                        case 6:
                            var UserWithLastProjectAndETC = await complexValueRequest.GetUserWithLastProjectAndETC();
                            if (UserWithLastProjectAndETC == null || UserWithLastProjectAndETC?.Count() == 0)
                            {
                                Console.WriteLine("No results");
                                break;
                            }
                            foreach (var tuple in UserWithLastProjectAndETC)
                            {
                                string taskInfo = "";
                                if (tuple.LastProjectTask != null)
                                {
                                    foreach (var task in tuple.LastProjectTask)
                                    {
                                        taskInfo += task.ToString() + " ";
                                    }
                                }
                                Console.WriteLine(string.Format(" Tuple: [ User:{0}, Last projects:{1}, Last project tasks:{2}, Count of finished or canceled tasks:{3}, Longest task:{4} ] ", tuple.User, tuple.LastProject, taskInfo, tuple.CountOfUnfinishedAndCanceledTasks, tuple.LongesUserTask));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 7:
                            var ProjectWithTaskWithLongestDescriptionsAndETC = await complexValueRequest.GetProjectWithTaskWithLongestDescriptionsAndETC();
                            if (ProjectWithTaskWithLongestDescriptionsAndETC == null || ProjectWithTaskWithLongestDescriptionsAndETC?.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in ProjectWithTaskWithLongestDescriptionsAndETC)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 8:
                            MarkTaskAsync();
                            break;
                        default:
                            Console.WriteLine("There are no item with this number.");
                            break;
                    }
                    
                }
                else
                {
                    Console.WriteLine("The entered value is not a valid numeric value.");
                }

                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Console.Clear();
            }
        }

        public static async System.Threading.Tasks.Task MarkTaskAsync()
        {
            while (true)
            {
                QueriesService queries = new QueriesService();
                var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
                Console.WriteLine(string.Format("Task id whose status has been changed to done: {0}", markedTaskId));
            }
        }
    }
}
