﻿using AutoMapper;
using Common.DTO;
using Common.MappingProfiles;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleClient.Reguests
{
    class UserRequest
    {
        public string ApiUrl = "https://localhost:44309";
        private Mapper _mapper;
        HttpClient httpClient;
        public UserRequest()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiUrl);
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();

            }));
        }
        public async Task<List<User>> GetUsersAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<User> userss = new List<User>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/User");
                if (response.IsSuccessStatusCode)
                {
                    userss = _mapper.Map<List<User>>(JsonConvert.DeserializeObject<List<UserDTO>>(await response.Content.ReadAsStringAsync()));
                }
                return userss;
            }
        }

        public async Task<User> GetUserAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                User user = new User();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/User/{0}", userId));
                if (response.IsSuccessStatusCode)
                {
                    user = _mapper.Map<User>(JsonConvert.DeserializeObject<UserDTO>(await response.Content.ReadAsStringAsync()));
                }
                return user;
            }
        }
    }
}
