﻿using AutoMapper;
using Common.DTO;
using Common.MappingProfiles;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ConsoleClient.Reguests
{
    public class TaskRequest
    {
        private string ApiUrl = "https://localhost:44309";
        private Mapper _mapper;
        HttpClient httpClient;
        public TaskRequest()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiUrl);
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();

            }));
        }
        public async System.Threading.Tasks.Task<List<Task>> GetTasksAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<Task> tasks = new List<Task>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Task");
                if (response.IsSuccessStatusCode)
                {
                    tasks = _mapper.Map<List<Task>>(JsonConvert.DeserializeObject<List<TaskDTO>>(await response.Content.ReadAsStringAsync()));
                }
                return tasks;
            }
        }

        public async System.Threading.Tasks.Task<Task> GetTaskAsync(int taskId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Task/{0}", taskId));
                if (response.IsSuccessStatusCode)
                {
                    return _mapper.Map<Task>(JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync()));
                }
                return null;
            }
        }

        public async System.Threading.Tasks.Task<Task> UpdateTaskAsync(Task task)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.PutAsync(string.Format("/api/Task/"), new StringContent(JsonConvert.SerializeObject(_mapper.Map<TaskDTO>(task)), Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    return _mapper.Map<Task>(JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync()));
                }
                return null;
            }
        }
    }
}
