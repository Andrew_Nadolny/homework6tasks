using AutoMapper;
using Common.DTO;
using Common.MappingProfiles;
using Common.Models;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Queries;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace HomeWork3ProjectStructure.BL.Tests
{
    public class BLTests : IDisposable
    {
        public QuerieHandler querieHandler;
        public CommandHandler commandHandler;

        DbContextOptions<ProjectsDbContext> dbContextOptions = new DbContextOptionsBuilder<ProjectsDbContext>().UseInMemoryDatabase("ProjectDataBaseForUnitTests").Options;

        IMapper mapper = new Mapper(new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<TaskProfile>();
            cfg.AddProfile<UserProfile>();

        }));
        ProjectsDbContext fakeProjectsDbContext;
        public BLTests()
        {
            fakeProjectsDbContext = GetContextWithData();
            querieHandler = new QuerieHandler(fakeProjectsDbContext, mapper);
            commandHandler = new CommandHandler(fakeProjectsDbContext, mapper);

        }
        private ProjectsDbContext GetContextWithData()
        {
            ProjectsDbContext projectsDbContext = new ProjectsDbContext(dbContextOptions);

            projectsDbContext.AddAsync(new User() { Id = 1, TeamId = 10, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") });
            projectsDbContext.AddAsync(new Team() { Id = 10, ProjectId = 1, Name = "Denesik - Greenfelder", CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00") });
            projectsDbContext.AddAsync(
                new Project()
                {
                    Id = 1,
                    AuthorId = 1,
                    TeamId = 10,
                    Name = "backing up Handcrafted Fresh Shoes challenge",
                    Description = "Et doloribus et temporibus.",
                    Deadline = Convert.ToDateTime("2021-09-12T19:17:47.2335223+00:00"),
                    CreatedAt = Convert.ToDateTime("2020-08-25T17:49:50.4518054+00:00")
                });
            projectsDbContext.AddRangeAsync(new List<Common.Models.Task>() { new Common.Models.Task() { Id = 1, ProjectId = 1, PerformerId = 1, Name = "real-time", Description = "Quo sint aut et ea voluptatem omnis ut.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 2, ProjectId = 1, PerformerId = 1,  Name = "product Direct utilize", Description = "Eum a eum.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2019-02-15T15:06:52.0600666+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 3, ProjectId = 1, PerformerId = 1, Name = "bypass", Description = "Sint voluptatem quas.", State = TaskState.Done, CreatedAt = Convert.ToDateTime("2017-08-16T06:13:44.5773845+00:00"), FinishedAt = Convert.ToDateTime("2021-05-09T06:34:47.460216+00:00") },
                    new Common.Models.Task() { Id = 4, ProjectId = 1, PerformerId = 1, Name = "withdrawal contextually-based", Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2018-10-19T00:58:34.8045103+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 5, ProjectId = 1, PerformerId = 1, Name = "mobile Organized", Description = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2018-06-15T06:03:48.0732466+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 6, ProjectId = 1, PerformerId = 1, Name = "world-class Circles", Description = "Reiciendis iusto rerum non et aut eaque.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2020-05-21T14:56:53.8117818+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 7, ProjectId = 1, PerformerId = 1, Name = "Automotive & Tools transitional bifurcated", Description = "Et rerum ad.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2018-07-10T16:21:12.0886153+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 8, ProjectId = 1, PerformerId = 1, Name = "payment methodologies", Description = "Voluptas nostrum sint.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2019-05-07T20:29:10.058295+00:00"), FinishedAt = Convert.ToDateTime(null) },
                    new Common.Models.Task() { Id = 9, ProjectId = 1, PerformerId = 1, Name = "Borders Mountain", Description = "Rerum iure soluta consequatur velit aut.", State = TaskState.InProgress, CreatedAt = Convert.ToDateTime("2020-06-19T11:42:55.0738847+00:00"), FinishedAt = Convert.ToDateTime(null) } });
            projectsDbContext.SaveChangesAsync();
            return projectsDbContext;
        }
        public void Dispose()
        {
            fakeProjectsDbContext.Database.EnsureDeleted();
            fakeProjectsDbContext.SaveChanges();
            fakeProjectsDbContext.Dispose();
        }
        [Fact]
        public async System.Threading.Tasks.Task GetCountTasksInProjectByAuthorIdQuerie_WhenAuthorWithThisIdUnexsist_ThenEmptyDictonatyAsync()
        {
            GetCountTasksInProjectByAuthorIdQuerie getCountTasksInProjectByAuthorIdQuerie = new GetCountTasksInProjectByAuthorIdQuerie();
            getCountTasksInProjectByAuthorIdQuerie.Id = 2;
            var answerDictionary =await querieHandler.HandleAsync(getCountTasksInProjectByAuthorIdQuerie);

            Assert.Equal(new Dictionary<int, int>(), answerDictionary);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetCountTasksInProjectByAuthorIdQuerie_WhenAuthorWithThisIdExsist_ThenDictonatyWithOnePairAsync()
        {
            GetCountTasksInProjectByAuthorIdQuerie getCountTasksInProjectByAuthorIdQuerie = new GetCountTasksInProjectByAuthorIdQuerie();
            getCountTasksInProjectByAuthorIdQuerie.Id = 1;
            var answerDictionary = await querieHandler.HandleAsync(getCountTasksInProjectByAuthorIdQuerie);

            var expactedDictionary = new Dictionary<int, int>();
            expactedDictionary.Add(1, 9);
            Assert.Equal(expactedDictionary, answerDictionary);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetListOfTeamsWithTeamatesOlderThen10Querie_WhenUserDontExist_ThenEmptyListAsync()
        {
            var team = fakeProjectsDbContext.Teams.SingleOrDefault(x => x.Id == 10);
            fakeProjectsDbContext.Teams.Remove(team);
            await fakeProjectsDbContext.SaveChangesAsync();

            var answerTeams = await querieHandler.HandleAsync(new GetListOfTeamsWithTeamatesOlderThen10Querie());

            Assert.Empty(answerTeams);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetListOfTeamsWithTeamatesOlderThen10Querie_WhenUserExist_ThenListOfTeamsAsync()
        {
            var answerTeams = await querieHandler.HandleAsync(new GetListOfTeamsWithTeamatesOlderThen10Querie());
            var expactedTeams =  new List<(int Id, string Name, List<UserDTO> Teammates)>();
            expactedTeams.Add((10, "Denesik - Greenfelder", new List<UserDTO>() { new UserDTO() { Id = 1, TeamId = 10, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") } }));

            Assert.Equal(expactedTeams, answerTeams);
        }

        [Fact]
        public void AddNewUser_WhenUserExist_ThenThrowArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(() => commandHandler
                .HandlerAsync(
                new CreateUserCommand()
                {
                    user = new UserDTO()
                    {
                        Id = -1,
                        TeamId = 10,
                        Name = "Brandy",
                        Surname = "Witting",
                        Email = "Brandy.Witti2ng@gmail.com",
                        RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"),
                        BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00")
                    }
                }
                ));
        }

        [Fact]
        public void AddNewUser_WhenTeamNotExist_ThenThrowArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(() => commandHandler
                .HandlerAsync(
                new CreateUserCommand()
                {
                    user = new UserDTO()
                    {
                        Id = 2,
                        TeamId = 11,
                        Name = "Brandy",
                        Surname = "Witting",
                        Email = "Brandy.Witti2ng@gmail.com",
                        RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"),
                        BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00")
                    }
                }
                ));
        }

        [Fact]
        public async System.Threading.Tasks.Task AddNewUser_WhenUserUnexist_ThenReturNewUserAsync()
        {
            var newUser = new UserDTO()
            {
                Id = 3,
                TeamId = 10,
                Name = "Brandy",
                Surname = "Witting",
                Email = "Brandy.Witti2ng@gmail.com",
                RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"),
                BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00")
            };

            var actialUser = await commandHandler.HandlerAsync(new CreateUserCommand() { user = newUser });
            Assert.True(newUser.Equals(actialUser));
        }

        [Fact]
        public void UpgrateTaskState_WhenTaskUnexist_ThenArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(() => commandHandler.HandlerAsync(new UpdateTaskCommand() { task = new TaskDTO() { Id = 10, State = 3, FinishedAt = DateTime.Now } }));
        }

        [Fact]
        public async System.Threading.Tasks.Task UpgrateTaskState_WhenTaskExist_ThenReturnUpgratedTask()
        {
            var task = new TaskDTO() { Id = 3, ProjectId = 1, PerformerId = 1, Name = "bypass", Description = "Sint voluptatem quas.", State = 3, CreatedAt = Convert.ToDateTime("2017-08-16T06:13:44.5773845+00:00"), FinishedAt = DateTime.Now };

            var actialTask = await commandHandler.HandlerAsync(new UpdateTaskCommand() { task = task });
            Assert.True(actialTask.Id == task.Id && actialTask.State == 3);
        }
        [Fact]
        public async System.Threading.Tasks.Task AddUserInTeam_WhenUserExist_ThenReturnUpgratedUser()
        {
            fakeProjectsDbContext.Teams.Add(new Team() { Id = 1, ProjectId = 1, Name = "Denesik - Greenfelder", CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00") });
            await fakeProjectsDbContext.SaveChangesAsync();
            var user = new UserDTO() { Id = 1, TeamId = 1, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") };

            var actialUser = await commandHandler.HandlerAsync(new UpdateUserCommand() { user = user });
            Assert.True(actialUser.Id == user.Id && actialUser.TeamId == 1);
        }
        [Fact]
        public async System.Threading.Tasks.Task GetListOfUsersAscendingWhitTasksDescendingQuerie_WhenUserUnexisits_TheReturnEmptyListAsync()
        {
            fakeProjectsDbContext.Users.Remove(fakeProjectsDbContext.Users.Single(x => x.Id == 1));
            fakeProjectsDbContext.SaveChanges();


            Assert.Empty(await querieHandler.HandleAsync(new GetListOfUsersAscendingWhitTasksDescendingQuerie()));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetListOfUsersAscendingWhitTasksDescendingQuerie_WhenTeamsUnexisits_TheReturnListWithOutTaskAsync()
        {
            fakeProjectsDbContext.Tasks.RemoveRange(fakeProjectsDbContext.Tasks.Where(x => x.PerformerId == 1));
            fakeProjectsDbContext.SaveChanges();

            var expactedList = new List<(UserDTO User, List<TaskDTO> Tasks)>() { (
                new UserDTO() {
                    Id = 1,
                    TeamId = 10,
                    Name = "Brandy",
                    Surname = "Witting",
                    Email = "Brandy.Witting@gmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"),
                    BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00")
                }, new List<TaskDTO>() ) };
            var answeredList = await querieHandler.HandleAsync(new GetListOfUsersAscendingWhitTasksDescendingQuerie());
            Assert.Equal(expactedList, answeredList);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetProjectWithTaskWithLongestDescriptionsAndETCQuerie_WhenProjectUnexsist_ThenListEmptyAsync()
        {
            fakeProjectsDbContext.Projects.RemoveRange(fakeProjectsDbContext.Projects.Where(x => x.Id == 1));
            fakeProjectsDbContext.SaveChanges();

            Assert.Empty(await querieHandler.HandleAsync(new GetProjectWithTaskWithLongestDescriptionsAndETCQuerie()));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetProjectWithTaskWithLongestDescriptionsAndETCQuerie_WhenUserUnexsist_ThenCountOfUserZeroAsync()
        {
            fakeProjectsDbContext.Users.RemoveRange(fakeProjectsDbContext.Users.Where(x => x.Id == 1));
            fakeProjectsDbContext.SaveChanges();

            Assert.Collection(await querieHandler.HandleAsync(new GetProjectWithTaskWithLongestDescriptionsAndETCQuerie()),
                item => Assert.Equal(0, item.CountOfUsersInProjects));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserTasksFinishedInCurrentYearQuerie_WhenTasksFinisheInThisUnexsist_ThenCountOfUserZeroAsync()
        {
            fakeProjectsDbContext.Tasks.RemoveRange(fakeProjectsDbContext.Tasks.Where(x => x.FinishedAt.Year == DateTime.Now.Year));
            fakeProjectsDbContext.SaveChanges();

            Assert.Empty(await querieHandler.HandleAsync(new GetUserTasksFinishedInCurrentYearQuerie() { Id = 1 }));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserTasksWithShortNameByUserIdQuerie_WhenTasksWithShortNamesUnexsist_ThenCountOfUserZeroAsync()
        {
            fakeProjectsDbContext.Tasks.RemoveRange(fakeProjectsDbContext.Tasks.Where(x => x.Name.Length < 45));
            fakeProjectsDbContext.SaveChanges();

            var list = await querieHandler.HandleAsync(new GetUserTasksWithShortNameByUserIdQuerie() { Id = 1 });

            Assert.Empty(list);
        }

        [Fact]
        public async void GetUserTasksWithShortNameByUserIdQuerie_WhenUsersUnexsist_ThenCountOfUserZero()
        {
            fakeProjectsDbContext.Users.RemoveRange(fakeProjectsDbContext.Users.Where(x => x.Id == 1));
            fakeProjectsDbContext.SaveChanges();

            var list = await querieHandler.HandleAsync(new GetUserTasksWithShortNameByUserIdQuerie() { Id = 1 });

            Assert.Empty(list);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserWithLastProjectAndETCQuerie_WhenProjectsEmpry_ThenUserWithoutAsync()
        {
            fakeProjectsDbContext.Projects.RemoveRange(fakeProjectsDbContext.Projects.Where(x => x.Id == 1));
            fakeProjectsDbContext.SaveChanges();

            var list = await querieHandler.HandleAsync(new GetUserWithLastProjectAndETCQuerie());
            var user = new UserDTO() { Id = 1, TeamId = 10, Name = "Brandy", Surname = "Witting", Email = "Brandy.Witting@gmail.com", RegisteredAt = Convert.ToDateTime("2019-01-10T02:51:56.7148896+00:00"), BirthDay = Convert.ToDateTime("2007-01-01T05:10:18.898869+00:00") };
            Assert.Collection(list,
                item =>
                {
                    Assert.Equal(user, item.User);
                    Assert.Null(item.LastProject);
                    Assert.Equal(0, item.CountOfUnfinishedAndCanceledTasks);
                    Assert.Null(item.LongesUserTask);
                    Assert.Empty(item.LastProjectTask);
                });
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserUnexsist_ThenArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await querieHandler.HandleAsync(new GetUserUnfinishedTasks() { Id = 2 }));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserExsist_ThenListTasksAsync()
        {
            Assert.True((await querieHandler.HandleAsync(new GetUserUnfinishedTasks() { Id = 1 })).All(x => x.State != 2));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserTaskUnexisit_ThenAggregateExceptionAsync()
        {
            fakeProjectsDbContext.Tasks.RemoveRange(fakeProjectsDbContext.Tasks.Where(x => x.PerformerId == 1));
            fakeProjectsDbContext.SaveChanges();

            await Assert.ThrowsAsync<AggregateException>(async () => await querieHandler.HandleAsync(new GetUserUnfinishedTasks() { Id = 1 }));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenUserUnfinishedTaskUnexsis_ThenEmptyListAsync()
        {
            fakeProjectsDbContext.Tasks.RemoveRange(fakeProjectsDbContext.Tasks.Where(x => x.PerformerId == 1 && x.State != TaskState.Done));
            fakeProjectsDbContext.SaveChanges();

            Assert.Empty(await querieHandler.HandleAsync(new GetUserUnfinishedTasks() { Id = 1 }));
        }
    }
}
